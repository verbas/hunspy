#!/usr/bin/env python3

from io import StringIO
from sys import exit
from unittest import TestCase, TestLoader, TextTestRunner

from coverage import Coverage


class TestMunch(TestCase):

    def _test_munch(self, aff, dic, expressions):
        _aff, _dic = StringIO(), StringIO()
        from hunspy import munch
        munch(_aff, _dic, expressions)
        _aff.seek(0), _dic.seek(0)
        self.assertEqual(aff, _aff.read())
        self.assertEqual(dic, _dic.read())

    def test_multiple_words(self):
        self._test_munch('SET UTF-8\n', '2\na\nb\n', ('a', 'b',))

    def test_multiword_expressions(self):
        self._test_munch('SET UTF-8\n', '2\na\nb\n', ('a b',))

    def test_no_expressions(self):
        self._test_munch('', '', ())

    def test_single_word(self):
        self._test_munch('SET UTF-8\n', '1\na\n', ('a',))


def main():
    coverage = Coverage(source=('hunspy',))
    coverage.start()
    test_suite = TestLoader().loadTestsFromTestCase(TestMunch)
    test_result = TextTestRunner().run(test_suite)
    exit_code = 0
    if not test_result.wasSuccessful():
        exit_code = -1
    coverage.stop()
    complete_coverage = True
    for file_path in coverage.get_data().measured_files():
        _, total, _, missing, lines = coverage.analysis2(file_path)
        if missing:
            total, missing = len(total), len(missing)
            if complete_coverage:
                complete_coverage = False
                print('FAILED: Uncomplete coverage')
            print('{}:  {:.2f}% (lines {})'.format(
                file_path, (total-missing)/total*100., lines))
            exit_code = -1
    exit(exit_code)


if __name__ == '__main__':
    main()

