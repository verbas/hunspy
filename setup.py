# coding=utf-8

import os.path
from setuptools import setup
from subprocess import check_output


_folder_path = os.path.dirname(__file__)
with open(os.path.join(_folder_path, 'requirements.txt')) as f:
    requirements = f.read().splitlines()
git_url = 'https://gitlab.com/verbas/hunspy'
version = check_output(
    ['git', 'describe', '--abbrev=0', '--tags']).strip().decode().lstrip('v')

setup(
    name='hunspy',
    version=version,
    description="API to work with Hunspell files.",
    url='http://verbas.rtfd.io/',
    download_url='{}/repository/archive.tar.gz?ref=v{}'.format(
        git_url, version),
    author="Adrián Chaves (Gallaecio)",
    author_email='adriyetichaves@gmail.com',
    license='AGPLv3+',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3 or '
            'later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Text Processing :: Linguistic',
    ],
    packages=['hunspy'],
    install_requires=requirements,
)
