#!/usr/bin/env python3
# coding=utf-8

import os
from subprocess import check_output
import sys


_parent_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')
sys.path.insert(0, _parent_folder)

extensions = [
    'sphinx.ext.autodoc',
]
templates_path = ['_templates']
source_suffix = ['.rst']
master_doc = 'index'
project = 'Verbas'
copyright = "2017, Adrián Chaves (Gallaecio)"
author = "Adrián Chaves (Gallaecio)"
version = check_output(
    ['git', 'describe', '--abbrev=0', '--tags']).strip().decode().lstrip('v')
release = version
language = None
exclude_patterns = ['_build']
pygments_style = 'sphinx'
todo_include_todos = False
html_theme = 'alabaster'
html_static_path = ['_static']
htmlhelp_basename = 'verbasdoc'
latex_elements = {}
latex_documents = [
    (master_doc, 'verbas.tex',
     'Verbas Documentation',
     "Adrián Chaves (Gallaecio)", 'manual'),
]
man_pages = [
    (master_doc, 'verbas', 'Verbas Documentation',
     [author], 1)
]
texinfo_documents = [
    (master_doc, 'verbas', 'Verbas Documentation',
     author, 'verbas', 'API to work with Hunspell files.',
     'Miscellaneous'),
]
