Hunspy
======

API to work with Hunspell files.


Requirements
------------

Hunspy only supports Python 3, it does not support Python 2.


Installation
------------

Installing with `pip <https://pip.pypa.io/en/stable/quickstart/>`_::

    pip install chakraversiontracker

Installing from sources::

    python setup.py install

Installing from sources for development (if you plan to extend tracked
packages)::

    python setup.py develop


Basic Usage
-----------

The :code:`munch()` function can build a pair of Hunspell files from an
iterable of words or multi-word expressions:

.. code-block:: python

    >>> from hunspy import munch
    >>> with open('a.aff', 'w') as aff:
    ...     with open('a.dic', 'w') as dic:
    ...         munch(aff, dic, ('foo', 'bar'))
    ...
    >>> print(open('a.aff').read())
    SET UTF-8

    >>> print(open('a.dic').read())
    2
    foo
    bar


Documentation
-------------

The documentation is available online at
`ReadTheDocs <http://verbas.rtfd.io/>`_.

You can also build it from sources:

#.  Install the requirements:

    .. code-block:: bash

        python3 -m venv venv
        . venv/bin/activate
        pip install -r requirements.txt -r requirements-doc.txt

#.  Build the documentation:

    .. code-block:: bash

        cd doc
        make html

#.  Open the documentation:

    .. code-block:: bash

        xdg-open _build/html/index.html


API Reference
-------------

:mod:`hunspy`
=============

..  automodule:: hunspy
    :members:


Contributing
------------

To work on Hunspy, install the packages in :code:`requirements-dev.txt`:

.. code-block:: bash

    python3 -m venv venv
    . venv/bin/activate
    pip install -r requirements.txt -r requirements-dev.txt

The source code is at :code:`hunspy`. Hack away!

After you make any change that you plan to submit upstream, please run the
automated tests:

.. code-block:: bash

    ./test.py

Tests will fail if your changes break existing tests or do not provide complete
coverage.


Credits and License
-------------------

Hunspy may be used under the terms of the :doc:`GNU Affero General Public
License version 3 </license>` or later (AGPLv3+).

For a list of authors who should be credited, see :doc:`/authors`.

.. toctree::
   :hidden:

   self
   license
   authors
