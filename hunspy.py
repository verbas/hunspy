import re
from tempfile import TemporaryFile


_DIC_WORD_SEPARATOR_RE = re.compile(r'\W+')


def munch(aff, dic, expressions):
    """Builts into the specified :code:`dic` and :code:`aff` files a Hunspell
    spellchecker that supports only the specified expressions.

    :code:`dic` and :code:`aff` must be file-like objects open for writing with
    their write cursor at their initial position.

    :code:`expressions` must be an iterable of words or multi-word expressions.

    .. note:: Hunspell can only spellcheck on a word-per-word basis. Hence,
              the spelling of multi-word expressions is not really checked,
              what is checked is the spelling of each word that makes up the
              multi-word expression.

              Words that are only allowed as part of a multi-word expression
              but not on their own will be accepted by the generated
              spellchecker. For example, if :code:`expressions` yields
              :code:`'foo bar'`, the resulting spellchecker will not only allow
              :code:`foo bar`, it will also allow :code:`foo` and :code:`bar`.

              You can not work around this limitation in any way. If you need
              the words of a multi-word expression to be allowed only when they
              appear together and in the correct order within a text, you need
              to use a grammar checker (e.g. LanguageTool) instead of a
              spellchecker like Hunspell.
    """
    with TemporaryFile('w+') as file:
        word_count = 0
        for expression in expressions:
            for word in _DIC_WORD_SEPARATOR_RE.split(expression):
                file.write(word + '\n')
                word_count += 1
        if not word_count:
            return
        aff.write('SET UTF-8\n')
        dic.write(str(word_count) + '\n')
        file.seek(0)
        for line in file:
            dic.write(line)
